module.exports = {
  apps : [
    {
      name: "musnas-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "musnas-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "musnas-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
