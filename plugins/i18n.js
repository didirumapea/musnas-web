import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
export default ({ app, store}) => {
  app.i18n = new VueI18n({
    locale: 'en',
    fallbackLocale: 'en',
    silentTranslationWarn: true,
    messages: {
      'en': {
        'general': {
          'perorangan': 'Individual',
          'grup': 'Group',
          'selanjutnya': 'Next',
          'bahasa_indonesia': 'Indonesian',
          'bahasa_inggris': 'English',
        },
        'home': {
          'title': 'Indonesian National Museum',
          'subtitle': 'Sharing Stories from Across the Country',
          'kewarganegaraan': 'Citizenship',
          'pilih_kewarganegaraan': 'Select nationality',
          'pilih_negara': 'Select country',
          'domestik': 'Domestic',
          'mancanegara': 'Foreign',
          'jenis_tiket': 'Ticket type',
          'pilih_jenis_tiket': 'Select the ticket type',
          'negara': 'Country',
        },
        'booking': {
          'identitas_pemesan': 'Customer Identity',
          'nama_pemesan': 'Order Name',
          'alamat_email': 'Email Address',
          'instansi': 'Agency',
          'kota': 'City',
          'tanggal': 'Date',
          'shift': 'Shift',
          'daftar_peserta': 'List of Participants',
          'kategori': 'Category',
          'jumlah_tiket': 'Number of Ticket',
          'tambah_peserta': 'Add Participants',
          'pembayaran': 'Payment',
          'peserta_masih_kosong': 'Participants are still empty',
          'title_booking': 'Order tickets online for the Indonesian National Museum, at low prices here now!',
          'p_nama_anda': 'Your Name',
          'p_email': 'You@gmail.com',
          'p_kota': 'Your City',
          'p_instansi': 'Your Agency',
          'p_date': 'August 31, 2020 12:00 PM',
          'pilih_shift': 'Select Shift',
          'pilih_category': 'Select Category',
        },
        'lang':{
          'bahasa': 'Language',
        },
      },
      'id': {
        'general': {
          'perorangan': 'Perorangan',
          'grup': 'Grup',
          'selanjutnya': 'Selanjutnnya',
          'bahasa_indonesia': 'Bahasa Indonesia',
          'bahasa_inggris': 'English',
        },
        'home': {
          'title': 'Museum Nasional Indonesia',
          'subtitle': 'Berbagi Kisah dari Seluruh Penjuru Negeri',
          'kewarganegaraan': 'Kewarganegaraan',
          'pilih_kewarganegaraan': 'Pilih Kewarganegaraan',
          'pilih_negara': 'Pilih Negara',
          'domestik': 'Domestik',
          'mancanegara': 'Mancanegara',
          'jenis_tiket': 'Jenis Tiket',
          'pilih_jenis_tiket': 'Pilih Jenis Tiket',
          'negara': 'Negara',
        },
        'booking': {
          'identitas_pemesan': 'Identitas Pemesan',
          'nama_pemesan': 'Nama Pemesan',
          'alamat_email': 'Alamat Email',
          'instansi': 'Instansi',
          'kota': 'Kota',
          'tanggal': 'Tanggal',
          'shift': 'Shift',
          'daftar_peserta': 'Daftar Peserta',
          'kategori': 'Kategori',
          'jumlah_tiket': 'Jumlah Tiket',
          'tambah_peserta': 'Tambah Peserta',
          'pembayaran': 'Pembayaran',
          'peserta_masih_kosong': 'Peserta masih kosong',
          'title_booking': 'Pesan tiket online museum nasional indonesia, dengan harga murah di sini sekarang juga !',
          'p_nama_anda': 'Nama Anda',
          'p_email': 'kamu@gmail.com',
          'p_kota': 'Kota Anda',
          'p_instansi': 'Instansi Anda',
          'p_date': 'Agustus 31, 2020 12:00 PM',
          'pilih_shift': 'Pilih Shift',
          'pilih_category': 'Pilih Kategori',
        },
        'lang':{
          'bahasa': 'Bahasa',
        },
      },
    },
  })
// })
//app.i18n.locale = 'bn'
  app.i18n.path = (link) => {
    if (app.i18n.locale === app.i18n.fallbackLocale) {
      return `/${link}`
    }
    return `/${app.i18n.locale}/${link}`
  }
}
