import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR }) => {
  if (isHMR) return

  // window.onNuxtReady(() => {
  createPersistedState({
    key: 'musnas2k20', // key
    // paths: ['auth.count']
    paths: ['helper.name', 'i18n'] // store file name
  })(store)
  // })
}
