import Vue from 'vue'
import VueQrcode from '@chenfengyuan/vue-qrcode';

const Navbar = () => import(/* webpackChunkName: "navbar" */ '@/components/core/Navbar.vue')
const Footer = () => import(/* webpackChunkName: "footer" */ '@/components/core/Footer.vue')

Vue.component('Navbar', Navbar)
Vue.component('Footer', Footer)
Vue.component('qrcode', VueQrcode)
