import Vue from 'vue'
// import { mapState } from 'vuex'
import { localize } from 'vee-validate';
const randomstring = require("randomstring");

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
Vue.mixin({
  methods: {
    localize (localeName) {
      // localize your app here, like i18n plugin.
      // asynchronously load the locale file then localize the validator with it.
      import(`vee-validate/dist/locale/${localeName+'.json'}`).then(locale => {
        localize(localeName, locale);
      });
    },
    generateInvoice(){
      const rand = randomstring.generate({
        length: 5,
        charset: 'numeric'
      });
      return '' + rand + this.$moment().unix()
      // console.log('' + rand + this.$moment().unix())
    },
    generateOrder(ticket_type){
      const rand = randomstring.generate({
        length: 5,
        charset: 'numeric'
      });
      return '' + this.ticketType(ticket_type) + this.$moment().unix() + rand
      // console.log('' + rand + this.$moment().unix())
    },
    ticketType(ticket_type){
      switch (ticket_type) {
        case 'individual':
          return 10;
        case 'group':
          return 11;
        default:
          console.log('not found id ticket')
          return '';
      }
    },
    randomNumber(length){
      return randomstring.generate({
        length: length,
        charset: 'numeric'
      });
    },
    ticketTypeId (ticket_type) {
      switch (ticket_type) {
        case 'individual':
          return 10;
        case 'group':
          return 11;
        default:
          console.log('not found id ticket')
          return '';
      }
    },
    categoryId (category) {
      switch (category) {
        case 'PAUD-TK':
          return {id: 1, price: 2000};
        case 'SD':
          return {id: 2, price: 2000};
        case 'SMP':
          return {id: 3, price: 2000};
        case 'SMA':
          return {id: 4, price: 2000};
        case 'Mahasiswa':
          return {id: 5, price: 5000};
        case 'Dewasa':
          return {id: 5, price: 5000};
        default:
          // FOREIGN
          return {id: 6, price: 10000};
      }
    },
    shuffleArray(a){
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    },
    convertIsoToDate(_date){
      // let date = new Date('2013-08-03T02:00:00Z');
      // let year = date.getFullYear();
      // let month = date.getMonth()+1;
      // let dt = date.getDate();
      // var str = '2011-04-11T10:20:30Z';
      let date = moment(_date);
      // let dateComponent = date.utc().format('DD MMMM YYYY');
      // let timeComponent = date.utc().format('HH:mm:ss');
      // console.log(dateComponent);
      // console.log(timeComponent);
      return date.utc().format('DD MMM YYYY');
    },
    loadingMode(text, loadingStatus) {
      let loader = this.$loading.show({
          // Optional parameters
          // container: this.fullPage ? null : this.$refs.baselayout,
          container: this.$refs.baselayout,
          // container: null,
          canCancel: false,
          onCancel: this.onCancel,
          transition: 'fade',
          // isFullPage: false,
          // opacity: 0.8
        },{
          // Pass slots by their names
          default: this.$createElement(eduLoader),
        }
      );
      this.$store.commit('loading/setTextLoading', {text:text, loader: loader})
      // simulate AJAX
      // setTimeout(() => {
      //   this.rLoader.hide()
      // }, 50000)
    },
    currencyFormat(n, currency){
        // WITH COMMA DIVIDER
        return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    },
    currencyFormat2(n, currency){
      // WITH DOTS DIVIDER
      return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
      });
    },
    countNilai(jmlsoal, jwbnBenar){

        return ('0000000000'+n).slice(-l);
      },
    moduleCategory(data){
        switch (data) {
            case 1:
            return 'bg-gold'
            case 2:
            return 'bg-blue'
            case 4:
            return 'bg-brown'
          }
    },
    moduleCategory2(data){
      switch (data) {
        case 1:
          return 'ico label-crown-1'
        case 2:
          return 'ico label-crown-3'
        case 4:
          return 'ico label-crown-4'
      }
    },
    htmlEntitiesConverter(htmlEntities){
        // console.log(entities.decode('&lt;&gt;&quot;&apos;&amp;&copy;&reg;&#8710;')); // <>"'&&copy;&reg;∆
        return entities.decode(htmlEntities)
    },
    groupBy(list, keyGetter) {
      const map = new Map();
      list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
          map.set(key, [item]);
        } else {
          collection.push(item);
        }
      });
      return map;
    },
    getTgl(){
      let tgl = []
      for (let x = 1; x <= 31; x++){
        tgl.push(x)
      }
      // console.log(tgl)
      return tgl
    },
    getBulan(){
      return ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    },
    getTahun(){
      let dt = new Date()
      // console.log(dt.getFullYear())
      let thn = []
      for (let x = dt.getFullYear(); x >= dt.getFullYear()-100; x--){
        thn.push(x)
      }
      // console.log(thn)
      return thn
    },
  },
})
