/* eslint-disable */
import Vue from 'vue'

Vue.prototype.$InitScript = async function (_t) {
  const getAllScriptElement = document.querySelectorAll('script[jumpstart]')
  if (getAllScriptElement.length) {
    for (let i = 0; i < getAllScriptElement.length; i++) {
      getAllScriptElement[i].parentNode.removeChild(getAllScriptElement[i]);
    }
    _t.$InitScript()
  } else {
    [
      { src: '/assets/js/aos.js', body: true },
      { src: '/assets/js/clipboard.min.js', body: true },
      { src: '/assets/js/jquery.fancybox.min.js', body: true },
      { src: '/assets/js/flatpickr.min.js', body: true },
      { src: '/assets/js/flickity.pkgd.min.js', body: true },
      { src: '/assets/js/ion.rangeSlider.min.js', body: true },
      { src: '/assets/js/isotope.pkgd.min.js', body: true },
      { src: '/assets/js/jarallax.min.js', body: true },
      { src: '/assets/js/jarallax-video.min.js', body: true },
      { src: '/assets/js/jarallax-element.min.js', body: true },
      { src: '/assets/js/jquery.countdown.min.js', body: true },
      { src: '/assets/js/jquery.smartWizard.min.js', body: true },
      { src: '/assets/js/plyr.polyfilled.min.js', body: true },
      { src: '/assets/js/prism.js', body: true },
      { src: '/assets/js/scrollMonitor.js', body: true },
      { src: '/assets/js/smooth-scroll.polyfills.min.js', body: true },
      { src: '/assets/js/svg-injector.umd.production.js', body: true, ssr: false },
      { src: '/assets/js/twitterFetcher_min.js', body: true },
      { src: '/assets/js/typed.min.js', body: true },
      { src: '/assets/js/theme.js', body: true },
      { src: '/assets/js/smooth-scroll.polyfills.min.js', body: true }
    ].map((r) => {
      const script = document.createElement('script')
      script.setAttribute('src', r.src)
      script.setAttribute('jumpstart', '')
      document.head.appendChild(script)
    })
  }
}
