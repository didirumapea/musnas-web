require('dotenv').config();

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: ' %s - Museum Nasional',//pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
      { hid: 'description', name: 'description', content: 'Museum Nasional Indonesia' },
      { name: 'keywords', content: 'Beli Tiket Museum Nasional Indonesia' },
      { name: 'author', content: '02 Team' },
      { name: 'publisher', content: 'Museum Nasional' },
      // { name: 'robots', content: 'index, follow' },
      { name: 'language', content: 'id' },
      // { name: 'geo.country', content: 'id' },
      { name: 'content-language', content: 'ln-Id' },
      // { name: 'geo.placename', content: 'indonesia' },
      // { name: 'yandex-verification', content: 'd676c02336e4384c' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/assets/css/loaders/loader-pulse.css', type: 'text/css', media: 'all' },
      { rel: 'stylesheet', href: '/assets/css/theme.css', type: 'text/css', media: 'all' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Nunito:400,400i,600,700&display=swap' }
    ],
    script: [
      { src: '/assets/js/jquery.min.js', body: true },
      { src: '/assets/js/popper.min.js', body: true },
      { src: '/assets/js/bootstrap.js', body: true },
      { src: '/assets/js/aos.js', body: true },
      { src: '/assets/js/clipboard.min.js', body: true },
      { src: '/assets/js/jquery.fancybox.min.js', body: true },
      { src: '/assets/js/flatpickr.min.js', body: true },
      { src: '/assets/js/flickity.pkgd.min.js', body: true },
      { src: '/assets/js/ion.rangeSlider.min.js', body: true },
      { src: '/assets/js/isotope.pkgd.min.js', body: true },
      { src: '/assets/js/jarallax.min.js', body: true },
      { src: '/assets/js/jarallax-video.min.js', body: true },
      { src: '/assets/js/jarallax-element.min.js', body: true },
      { src: '/assets/js/jquery.countdown.min.js', body: true },
      { src: '/assets/js/jquery.smartWizard.min.js', body: true },
      { src: '/assets/js/plyr.polyfilled.min.js', body: true },
      { src: '/assets/js/prism.js', body: true },
      { src: '/assets/js/scrollMonitor.js', body: true },
      { src: '/assets/js/smooth-scroll.polyfills.min.js', body: true },
      { src: '/assets/js/svg-injector.umd.production.js', body: true, ssr: false },
      { src: '/assets/js/twitterFetcher_min.js', body: true },
      { src: '/assets/js/typed.min.js', body: true },
      { src: '/assets/js/theme.js', body: true },
      { src: '/assets/js/smooth-scroll.polyfills.min.js', body: true }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/main.css',
    '@/assets/scss/theme.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/components.js',
    '~plugins/maps.js',
    '~/plugins/func',
    { src: '~/plugins/axios.js' },
    { src: '~/plugins/script.js', ssr: false },
    { src: '~plugins/vee-validate.js', ssr: false },
    { src: '~/plugins/localStorage.js', ssr: false },
    { src: '~/plugins/i18n.js', ssr: false },
  ],
  // Server Area
  server: {
    port: process.env.WEB_PORT, // default: 3000
    host: process.env.WEB_HOST, // default: localhost,
    // host: '0.0.0.0', // default: localhost,
    // port: 2604, // default: 3000
    // host: '192.168.1.188', // default: localhost,
    // timing: false,
    // https: {
    // 	key: fs.readFileSync(path.resolve(__dirname, '.https/server.key')),
    // 	cert: fs.readFileSync(path.resolve(__dirname, '.https/server.crt'))
    // }
  },
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment',
  ],
  /*
  ** Nuxt.js router
  */
  router: {
    middleware: ['redirect'],
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    ['@nuxtjs/pwa', {
      baseURL: process.env.NODE_ENV === 'production' ? 'http://localhost:3000' : 'http://localhost:3000'
    }],
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/svg',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
// ---------- maunual setting ---------
    // proxyHeaders: false,
    // credentials: false,
    baseURL: process.env.NODE_ENV !== "production"
      // ? `http://192.168.43.116:3132/api/v1/`
      ? `http://localhost:2121/api/v1/`
      // ? `https://api.bumikita.or.id/`
      : "https://api.museumnasional.or.id/api/v1/",
    // ---------- proxy setting -----------
    // proxy: true, // Can be also an object with default options
    // prefix: '/api/'
  },
  manifest: {
    // name: 'Nuxt Html',
    // lang: 'id',
    // short_name: 'Nuxt Html',
    // start_url: '/',
    display: 'standalone',
    background_color: '#ffffff',
    theme_color: '#049b93',
    orientationSection: 'portrait',
    // description: 'Nuxt Html'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    // extend (config, ctx) {
    // }
    build: {
      extend: (config) => {
        const svgRule = config.module.rules.find(rule => rule.test.test('.svg'));

        svgRule.test = /\.(png|jpe?g|gif|webp)$/;

        config.module.rules.push({
          test: /\.svg$/,
          use: [
            'babel-loader',
            'vue-svg-loader',
          ],
        });
      },
    },
  },
  generate: {
    dir: "public"
  }
}
