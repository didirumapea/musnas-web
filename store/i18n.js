import i18n from '@/plugins/i18n';
// STATE AS VARIABLE
export const state = () => ({
    locale: 'id',
    citizenship: '',
    booking_no: '',
    country_id: '',
    ticket_type: ''
  })
// ACTIONS AS METHODS
export const actions = { // asyncronous
    async setEnglish({commit}, payload) {
      commit('buyModule', payload)
    },
    async setIndonesia({commit}) {
      commit('getOrderModule')
    },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  setStep1(state, payload){
    state.citizenship = payload.citizenship
    state.ticket_type = payload.citizenship === 'foreign' ? 'group' : payload.ticket_type
    state.booking_no = payload.booking_no
    state.country_id = payload.country_id
    // console.log(state)
  },
  setIndonesia(state, payload){
    this.app.i18n.locale = payload
    state.locale = payload
  },
  setEnglish(state, payload){
    this.app.i18n.locale = payload
    state.locale = payload
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {

}
