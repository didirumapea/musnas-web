
// STATE AS VARIABLE
export const state = () => ({
   country: ['indonesia']
  })
// ACTIONS AS METHODS
export const actions = { // asyncronous
    async getCountry({ commit }) {
     return await this.$axios.get('web/masterdata/country/list/page=1/limit=1000/column-sort=name/sort=asc')
          .then((response) => {
              // console.log(response.data.data)
              commit('setCountry', response.data.data)
              // return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err)
            return err.response
        })
      // commit('hapusSelectedCart', payload)
    },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
    setCountry(state, payload){
      state.country = payload
      // console.log(state.country)
    },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {

}
