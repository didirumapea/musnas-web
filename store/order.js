
// STATE AS VARIABLE
export const state = () => ({
    detailOrder: {
      name: '',
      email: '',
      agency: '',
      city: '',
      // citizenship: '',
      // ticket_type: '',
      // booking_no: '',
      invoice_no: '',
      order_no: '',
      payment_status: 'pending',
      list_ticket: [],
      price: 0,
      payment_url: 0,
    }
  })
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async orderTicket({commit}, payload) {
      // console.log(payload)
      return await this.$axios.post('web/users/order', payload)
        .then((response) => {
          // console.log(response)
          commit('setStep2', payload)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          //   console.log(err.response)
          return err.response
        })
      // commit('hapusSelectedCart', payload)
  },
  async getDetailOrder({commit}, payload) {
    return await this.$axios.get(`web/users/order-no=${payload}`)
      .then((response) => {
        // console.log(response.data_order)
        commit('setDetailOrder', response)
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))
          console.log(err.response)
        return err.response
      })
    // commit('hapusSelectedCart', payload)
  },
  async listCategory({commit}, payload) {
    return await this.$axios.get(`web/masterdata/category/list/ticket-type=${payload}/page=1/limit=1000/column-sort=name/sort=asc`)
      .then((response) => {
        // commit('setListCategory', response.data.data)
        return response.data.data
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))
          console.log(err.response)
        return err.response
      })
    // commit('hapusSelectedCart', payload)
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
    setStep2(state, payload){
      state.detailOrder.name = payload.name
      state.detailOrder.email = payload.email
      state.detailOrder.agency = payload.agency
      state.detailOrder.city = payload.city
      state.detailOrder.schedule_date = payload.schedule_date
      state.detailOrder.schedule_time = payload.schedule_time
      state.detailOrder.list_ticket = payload.listOrder
      state.detailOrder.invoice_no = payload.invoice_no
      state.detailOrder.price = payload.price
      delete state.detailOrder.listOrder
      delete state.detailOrder.totalPrice
      // console.log(state)
    },
    setListCategory(state, payload){
      state.listCategory = payload
      // console.log(payload)
    },
    setDetailOrder(state, payload){
      // console.log(payload.data.data[0])
      state.detailOrder.name = payload.data.data[0].name
        state.detailOrder.email = payload.data.data[0].email
        state.detailOrder.agency = payload.data.data[0].agency
        state.detailOrder.city = payload.data.data[0].city
        state.detailOrder.citizenship = payload.data.data[0].citizenship
        state.detailOrder.ticket_type = payload.data.data[0].ticket_type
        state.detailOrder.booking_no = payload.data.data[0].booking_no
        state.detailOrder.invoice_no = payload.data.data[0].invoice_no
        state.detailOrder.order_no = payload.data.data[0].order_no
        state.detailOrder.payment_status = payload.data.data[0].payment_status
        state.detailOrder.list_ticket = payload.data.list_ticket
        state.detailOrder.payment_url = payload.data.data[0].payment_url
        state.detailOrder.schedule_date = payload.data.data[0].schedule_date
        state.detailOrder.order_date = payload.data.data[0].created_at
      // console.log(state.detailOrder)
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {

}
